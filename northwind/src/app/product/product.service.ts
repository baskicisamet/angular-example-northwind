import { Injectable } from '@angular/core';
import {Product} from './product'
import {ProductList} from './product-list.mock'

@Injectable()
export class ProductService {

  constructor() { }

  getProducts(){
    return  ProductList;
  }

}
